This repository contains ontologies and test cases which will be used to evaluate and compare the performance of some algorithms.

The ontologies were downloaded from [BioPortal Snapshot 30.03.2017](https://zenodo.org/record/439510), extracted from [BioPortal](https://bioportal.bioontology.org/) and processed by Nicolas Matentzoglu and Bijan Parsia. 

OWL2DL-CCC is an ontology change case creator ([source](https://gitlab.com/rfguimaraes/owl2dl-ccc), by Ricardo F. Guimarães).

