#!/bin/bash

onto_dir=$1
case_dir=$2
out_dir=$3
iterations=$4
timeout=$5

show_usage(){
    echo -e 'Usage:\t./generate.sh <ontology-dir> <case-dir> <output-dir> <iterations> <timeout>'
}

if [[ $# -ne 5 ]]
then
    show_usage
    exit 64
fi


mkdir -p "$out_dir"
set -o pipefail
i=1
for f in ${onto_dir}/*.owl.xml
do
    onto=$(realpath "$f")
    ii=$(printf "%03d" "$i")
    name=$(basename "$onto" | head -c 9)
    for ff in ${case_dir}/*.json
    do
        template=$(realpath "$ff")
        template_name=$(basename "$template" | head -c -6)
        b="${ii}__${name}__${template_name}_"
        flagname="$out_dir/${b}.finished"
        output_name="$out_dir/${b}.output"
        if [ -f "${flagname}" ]
        then
            echo "Skipping ${b}: already completed."
        else
            echo "${b}..."
            timeout -k 1s "$((timeout + 1))s" ./owl2dl-ccc-0.0.8-alpha2/bin/owl2dl-ccc -s "$onto" -c "$template" -d "$out_dir" -i "$iterations" -n "$b" -r JFact -t "${timeout}" 2>&1 | tee "$output_name"
            if [[ $? -eq 124 ]]
            then
                echo 'Timeout' | tee -a "${output_name}"
            fi
            touch "${flagname}"

        fi

    done
    ((i++))
done
set +o pipefail
