@rem
@rem Copyright 2015 the original author or authors.
@rem
@rem Licensed under the Apache License, Version 2.0 (the "License");
@rem you may not use this file except in compliance with the License.
@rem You may obtain a copy of the License at
@rem
@rem      https://www.apache.org/licenses/LICENSE-2.0
@rem
@rem Unless required by applicable law or agreed to in writing, software
@rem distributed under the License is distributed on an "AS IS" BASIS,
@rem WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
@rem See the License for the specific language governing permissions and
@rem limitations under the License.
@rem

@if "%DEBUG%" == "" @echo off
@rem ##########################################################################
@rem
@rem  owl2dl-ccc startup script for Windows
@rem
@rem ##########################################################################

@rem Set local scope for the variables with windows NT shell
if "%OS%"=="Windows_NT" setlocal

set DIRNAME=%~dp0
if "%DIRNAME%" == "" set DIRNAME=.
set APP_BASE_NAME=%~n0
set APP_HOME=%DIRNAME%..

@rem Resolve any "." and ".." in APP_HOME to make it shorter.
for %%i in ("%APP_HOME%") do set APP_HOME=%%~fi

@rem Add default JVM options here. You can also use JAVA_OPTS and OWL2DL_CCC_OPTS to pass JVM options to this script.
set DEFAULT_JVM_OPTS=

@rem Find java.exe
if defined JAVA_HOME goto findJavaFromJavaHome

set JAVA_EXE=java.exe
%JAVA_EXE% -version >NUL 2>&1
if "%ERRORLEVEL%" == "0" goto execute

echo.
echo ERROR: JAVA_HOME is not set and no 'java' command could be found in your PATH.
echo.
echo Please set the JAVA_HOME variable in your environment to match the
echo location of your Java installation.

goto fail

:findJavaFromJavaHome
set JAVA_HOME=%JAVA_HOME:"=%
set JAVA_EXE=%JAVA_HOME%/bin/java.exe

if exist "%JAVA_EXE%" goto execute

echo.
echo ERROR: JAVA_HOME is set to an invalid directory: %JAVA_HOME%
echo.
echo Please set the JAVA_HOME variable in your environment to match the
echo location of your Java installation.

goto fail

:execute
@rem Setup the command line

set CLASSPATH=%APP_HOME%\lib\owl2dl-ccc-0.0.8-alpha2.jar;%APP_HOME%\lib\jfact-5.0.3.jar;%APP_HOME%\lib\org.semanticweb.hermit-1.4.3.517.jar;%APP_HOME%\lib\owlapi-distribution-5.1.11.jar;%APP_HOME%\lib\owlapi-compatibility-5.1.11.jar;%APP_HOME%\lib\owlapi-apibinding-5.1.11.jar;%APP_HOME%\lib\owlapi-impl-5.1.11.jar;%APP_HOME%\lib\owlapi-oboformat-5.1.11.jar;%APP_HOME%\lib\owlapi-rio-5.1.11.jar;%APP_HOME%\lib\owlapi-parsers-5.1.11.jar;%APP_HOME%\lib\owlapi-tools-5.1.11.jar;%APP_HOME%\lib\owlapi-api-5.1.11.jar;%APP_HOME%\lib\guava-28.0-jre.jar;%APP_HOME%\lib\jackson-datatype-jdk8-2.9.6.jar;%APP_HOME%\lib\jena-arq-3.9.0.jar;%APP_HOME%\lib\rdf4j-rio-jsonld-2.3.2.jar;%APP_HOME%\lib\jsonld-java-0.12.1.jar;%APP_HOME%\lib\jackson-databind-2.9.9.jar;%APP_HOME%\lib\rdf4j-rio-rdfjson-2.3.2.jar;%APP_HOME%\lib\jackson-core-2.9.9.jar;%APP_HOME%\lib\jena-core-3.9.0.jar;%APP_HOME%\lib\jena-base-3.9.0.jar;%APP_HOME%\lib\commons-lang3-3.9.jar;%APP_HOME%\lib\jcommander-1.78.jar;%APP_HOME%\lib\log4j-slf4j-impl-2.11.1.jar;%APP_HOME%\lib\log4j-core-2.11.1.jar;%APP_HOME%\lib\log4j-api-2.11.1.jar;%APP_HOME%\lib\rdf4j-rio-n3-2.3.2.jar;%APP_HOME%\lib\rdf4j-rio-nquads-2.3.2.jar;%APP_HOME%\lib\rdf4j-rio-ntriples-2.3.2.jar;%APP_HOME%\lib\rdf4j-rio-rdfxml-2.3.2.jar;%APP_HOME%\lib\rdf4j-rio-trix-2.3.2.jar;%APP_HOME%\lib\rdf4j-rio-trig-2.3.2.jar;%APP_HOME%\lib\rdf4j-rio-turtle-2.3.2.jar;%APP_HOME%\lib\rdf4j-rio-languages-2.3.2.jar;%APP_HOME%\lib\rdf4j-rio-datatypes-2.3.2.jar;%APP_HOME%\lib\rdf4j-rio-binary-2.3.2.jar;%APP_HOME%\lib\rdf4j-rio-api-2.3.2.jar;%APP_HOME%\lib\rdf4j-model-2.3.2.jar;%APP_HOME%\lib\rdf4j-util-2.3.2.jar;%APP_HOME%\lib\jcl-over-slf4j-1.7.25.jar;%APP_HOME%\lib\jena-shaded-guava-3.9.0.jar;%APP_HOME%\lib\libthrift-0.10.0.jar;%APP_HOME%\lib\jena-iri-3.9.0.jar;%APP_HOME%\lib\slf4j-api-1.7.25.jar;%APP_HOME%\lib\failureaccess-1.0.1.jar;%APP_HOME%\lib\listenablefuture-9999.0-empty-to-avoid-conflict-with-guava.jar;%APP_HOME%\lib\jsr305-3.0.2.jar;%APP_HOME%\lib\checker-qual-2.8.1.jar;%APP_HOME%\lib\error_prone_annotations-2.3.2.jar;%APP_HOME%\lib\j2objc-annotations-1.3.jar;%APP_HOME%\lib\animal-sniffer-annotations-1.17.jar;%APP_HOME%\lib\jackson-annotations-2.9.9.jar;%APP_HOME%\lib\commons-rdf-api-0.5.0.jar;%APP_HOME%\lib\xz-1.6.jar;%APP_HOME%\lib\hppcrt-0.7.5.jar;%APP_HOME%\lib\caffeine-2.6.1.jar;%APP_HOME%\lib\commons-io-2.6.jar;%APP_HOME%\lib\httpclient-osgi-4.5.6.jar;%APP_HOME%\lib\httpclient-cache-4.5.6.jar;%APP_HOME%\lib\httpmime-4.5.6.jar;%APP_HOME%\lib\fluent-hc-4.5.6.jar;%APP_HOME%\lib\httpclient-4.5.6.jar;%APP_HOME%\lib\trove4j-3.0.3.jar;%APP_HOME%\lib\joda-time-2.9.7.jar;%APP_HOME%\lib\RoaringBitmap-0.6.32.jar;%APP_HOME%\lib\axiom-c14n-1.2.14.jar;%APP_HOME%\lib\axiom-impl-1.2.14.jar;%APP_HOME%\lib\axiom-dom-1.2.14.jar;%APP_HOME%\lib\axiom-api-1.2.14.jar;%APP_HOME%\lib\commons-logging-1.2.jar;%APP_HOME%\lib\automaton-1.11-8.jar;%APP_HOME%\lib\java-getopt-1.0.13.jar;%APP_HOME%\lib\httpcore-osgi-4.4.10.jar;%APP_HOME%\lib\commons-cli-1.4.jar;%APP_HOME%\lib\commons-codec-1.11.jar;%APP_HOME%\lib\httpcore-nio-4.4.10.jar;%APP_HOME%\lib\httpcore-4.4.10.jar;%APP_HOME%\lib\geronimo-activation_1.1_spec-1.1.jar;%APP_HOME%\lib\geronimo-javamail_1.4_spec-1.7.1.jar;%APP_HOME%\lib\jaxen-1.1.4.jar;%APP_HOME%\lib\geronimo-stax-api_1.0_spec-1.0.1.jar;%APP_HOME%\lib\apache-mime4j-core-0.7.2.jar;%APP_HOME%\lib\woodstox-core-asl-4.1.4.jar;%APP_HOME%\lib\commons-csv-1.5.jar;%APP_HOME%\lib\commons-compress-1.17.jar;%APP_HOME%\lib\collection-0.7.jar;%APP_HOME%\lib\stax2-api-3.1.1.jar;%APP_HOME%\lib\javax.inject-1.jar


@rem Execute owl2dl-ccc
"%JAVA_EXE%" %DEFAULT_JVM_OPTS% %JAVA_OPTS% %OWL2DL_CCC_OPTS%  -classpath "%CLASSPATH%" br.usp.ime.owl2dlccc.App %*

:end
@rem End local scope for the variables with windows NT shell
if "%ERRORLEVEL%"=="0" goto mainEnd

:fail
rem Set variable OWL2DL_CCC_EXIT_CONSOLE if you need the _script_ return code instead of
rem the _cmd.exe /c_ return code!
if  not "" == "%OWL2DL_CCC_EXIT_CONSOLE%" exit 1
exit /b 1

:mainEnd
if "%OS%"=="Windows_NT" endlocal

:omega
