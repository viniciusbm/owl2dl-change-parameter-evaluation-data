#/bin/bash

for spec in $2/*.json; do
    specfile=$(basename -- "$spec")
    specname="${specfile%.*}"
    for onto in $1/*.owl.xml; do
        ontofilename=$(basename -- "$onto")
        ontoprefix=$(echo "$ontofilename" | awk -F '[.]' '{print $1}')
        echo $(date)
        echo $onto - $spec
        JAVAxOPTS="-Xms2G -Xmx10G"
        JAVA_OPTS="${JAVAxOPTS}" timeout -k 3800s 3700s ./owl2dl-ccc -r JFact -c $spec -d $3 -i 3 -n ${ontoprefix}.$4.${specname} -s $onto -t 3600
        echo ""
    done
done
